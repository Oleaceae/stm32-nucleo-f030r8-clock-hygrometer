#include "callbacks.h"

#include "keypad.h"
#include "clock.h"

extern TIM_HandleTypeDef htim14;
extern TIM_HandleTypeDef htim16;

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
	if (htim->Instance == htim14.Instance) {
		KP_multiplexing_switch();
	}
	else if (htim->Instance == htim16.Instance) {
		CLK_tick();
	}
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {
	KP_handle_keypress(GPIO_Pin);
}
