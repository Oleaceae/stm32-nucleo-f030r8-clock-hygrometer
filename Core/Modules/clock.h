#ifndef MODULES_CLOCK_H_
#define MODULES_CLOCK_H_

#include <stdint.h>

typedef uint32_t ux_timestamp_s;

enum CLK_Mode {
	CLK_MODE_FULL,
	CLK_MODE_AM_PM,
	CLK_MODE_RAW
};

ux_timestamp_s CLK_get();
void CLK_set(ux_timestamp_s time);
void CLK_tick();
char* CLK_get_formatted_system_time(char* buffer, enum CLK_Mode mode);
char* CLK_get_formatted_time(char* buffer, ux_timestamp_s timestamp, enum CLK_Mode mode);

#endif
