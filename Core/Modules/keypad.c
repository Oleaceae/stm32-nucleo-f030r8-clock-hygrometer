#include "keypad.h"

#include "pin_defines.h"

#define COL_ZERO 0
#define COL_ONE 1
#define COL_TWO 2
#define COL_THREE 3

static uint8_t key_to_keycode(unsigned int column, unsigned int row);

struct keypad_event {
	bool keyPressed;
	uint8_t keycode;
};

static struct keypad_event keypadEvent = {0};
static bool pause = {0};
static uint8_t activeColumn = {0};

void KP_debounce() {
	// unimplemented
}

bool KP_poll() {
	return keypadEvent.keyPressed;
}

uint8_t KP_consume_key() {
	keypadEvent.keyPressed = false;
	return keypadEvent.keycode;
}

void KP_handle_keypress(uint16_t GPIO_Pin) {
	keypadEvent.keyPressed = true;
	switch (GPIO_Pin) {
		case KEYPAD_ROW_0_Pin:
			keypadEvent.keycode = key_to_keycode(activeColumn, 0);
			break;
		case KEYPAD_ROW_1_Pin:
			keypadEvent.keycode = key_to_keycode(activeColumn, 1);
			break;
		case KEYPAD_ROW_2_Pin:
			keypadEvent.keycode = key_to_keycode(activeColumn, 2);
			break;
		case KEYPAD_ROW_3_Pin:
			keypadEvent.keycode = key_to_keycode(activeColumn, 3);
			break;
	}
}

void KP_multiplexing_switch() {
	if (pause) {
		pause = false;
		HAL_GPIO_WritePin(KEYPAD_COL_0_GPIO_Port, KEYPAD_COL_0_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(KEYPAD_COL_1_GPIO_Port, KEYPAD_COL_1_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(KEYPAD_COL_2_GPIO_Port, KEYPAD_COL_2_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(KEYPAD_COL_3_GPIO_Port, KEYPAD_COL_3_Pin, GPIO_PIN_RESET);
		return;
	}
	switch(activeColumn) {
		case COL_THREE:
			  HAL_GPIO_WritePin(KEYPAD_COL_0_GPIO_Port, KEYPAD_COL_0_Pin, GPIO_PIN_SET);
			  activeColumn = COL_ZERO;
			  break;
		case COL_ZERO:
			  HAL_GPIO_WritePin(KEYPAD_COL_1_GPIO_Port, KEYPAD_COL_1_Pin, GPIO_PIN_SET);
			  activeColumn = COL_ONE;
			  break;
		case COL_ONE:
			  HAL_GPIO_WritePin(KEYPAD_COL_2_GPIO_Port, KEYPAD_COL_2_Pin, GPIO_PIN_SET);
			  activeColumn = COL_TWO;
			  break;
		case COL_TWO:
			  HAL_GPIO_WritePin(KEYPAD_COL_3_GPIO_Port, KEYPAD_COL_3_Pin, GPIO_PIN_SET);
			  activeColumn = COL_THREE;
			  break;
	}
	pause = true;
}

static uint8_t key_to_keycode(unsigned int column, unsigned int row) {
	return (column | row << 2);
}
