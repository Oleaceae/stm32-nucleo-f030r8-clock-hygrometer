#ifndef MODULES_ONE_WIRE_H_
#define MODULES_ONE_WIRE_H_

#include <stdlib.h>
#include <stdbool.h>

#include "stm32f0xx_ll_gpio.h"
#include "stm32f0xx_hal.h"
#include "uart_send.h"
#include "delay_us.h"
#include "pin_defines.h"

bool OW_reset();
void OW_write_byte(uint8_t byte);
uint8_t OW_read_byte();

#endif
