#include "watchdog.h"

#include <stdlib.h>
#include <string.h>

#include "stm32f0xx_hal.h"

#define EMPTY_FLASH_MEMORY 0xFFFFFFFF

extern struct watchdog watchdogTemperature;
extern struct watchdog watchdogHumidity;

static struct watchdog empty_watchdog();
static uint32_t to_word(struct watchdog* dog);

void WD_save_to_flash() {
	HAL_FLASH_Unlock();

	FLASH_EraseInitTypeDef eraseConfig = {FLASH_TYPEERASE_PAGES, WD_FLASH_PAGE_ADDRESS, 1};
	uint32_t errorContainer = 0;
	HAL_FLASHEx_Erase(&eraseConfig, &errorContainer);

	HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, TEMPERATURE_WATCHDOG_ADDRESS, to_word(&watchdogTemperature));
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, HUMIDITY_WATCHDOG_ADDRESS, to_word(&watchdogHumidity));

	HAL_FLASH_Lock();
}

uint32_t to_word(struct watchdog* dog) {
	uint32_t data = 0;
	data = (data | dog->active) << 8;
	data = (data | dog->min) << 8;
	data = data | dog->max;
	return data;
}

struct watchdog WD_load_from_flash(uint32_t address) {
	if (address < WD_FLASH_PAGE_ADDRESS || address > WD_FLASH_MAXIMUM_ADDRESS) {
		return empty_watchdog();
	}

	HAL_FLASH_Unlock();

	uint32_t data = *(uint32_t*) address;

	HAL_FLASH_Lock();

	if (data == EMPTY_FLASH_MEMORY) {
		return empty_watchdog();
	}

	struct watchdog dog;
	dog.max = data & 0xFF;
	dog.min = data >> 8 & 0xFF;
	dog.active = data >> 16;

	return dog;
}

struct watchdog empty_watchdog() {
	struct watchdog dog = {0, 0, 0};
	return dog;
}

bool WD_within_range(struct watchdog* dog, int8_t value) {
	if (!dog->active) {
		return true;
	}
	return (value > dog->min && value < dog->max);
}

char* WD_get_formatted_values(char* buffer, struct watchdog* dog) {
	memset(buffer, 0, strlen(buffer));

	strcat(buffer, dog->active ? "ON  " : "OFF ");

	if (dog->min < 10) {
		strcat(buffer, "0");
	}
	char numBuf[4] = {'\0'};
	itoa(dog->min, numBuf, 10);
	strcat(buffer, numBuf);
	strcat(buffer, " ");

	strcat(buffer, "TO");
	strcat(buffer, " ");

	if (dog->max < 10) {
		strcat(buffer, "0");
	}
	memset(numBuf, 0, strlen(numBuf));
	itoa(dog->max, numBuf, 10);
	strcat(buffer, numBuf);

	return buffer;
}

bool WD_compare(struct watchdog* dog, struct watchdog* other) {
	return (dog->active == other->active && dog->max == other->max && dog->min == other->min);
}

int8_t WD_clamp(int8_t value) {
	if (value < 0) {
		return 0;
	}
	if (value > 99) {
		return 99;
	}
	return value;
}
