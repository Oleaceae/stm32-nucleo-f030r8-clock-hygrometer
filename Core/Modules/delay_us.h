#ifndef MODULES_DELAY_US_H_
#define MODULES_DELAY_US_H_

#include <stdint.h>

void delay_us(uint16_t microseconds);

#endif
