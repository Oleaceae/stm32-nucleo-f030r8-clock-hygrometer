#include "temperature_sensor.h"

#include "watchdog.h"

// Private defines
// Command defines for OW interface with DS18S20 temperature sensor
#define SKIP_ROM 0xCC
#define CONVERT_T 0x44
#define READ_SCRATCHPAD 0xBE

// Scratchpad size & bytes
#define SCRATCHPAD_SIZE 9
#define TEMP_LSB 0
#define TEMP_MSB 1
#define USER_BYTE_1 2
#define USER_BYTE_2 3
#define RESERVED_1 4
#define RESERVED_2 5
#define COUNT_REMAIN 6
#define COUNT_PER_C 7
#define CRC_CHECKSUM 8

#define MAX_TEMPERATURE 125
#define MIN_TEMPERATURE -55

struct watchdog watchdogTemperature;

float TS_read_temperature(UART_HandleTypeDef* debugOutput) {
	if (!OW_reset()) {
		UART_Send(debugOutput, "TS: Sensor did not respond to RESET");
		return 0;
	}
	OW_write_byte(SKIP_ROM);
	OW_write_byte(CONVERT_T);
	HAL_Delay(750); // Temperature conversion at 12 bit resolution will take up to 750ms
	LL_GPIO_SetPinMode(TS_GPIO_Port, TS_Pin, LL_GPIO_MODE_INPUT);

	if (!OW_reset()) {
		UART_Send(debugOutput, "TS: Sensor did not respond to RESET after CONVERT_T");
		return 0;
	}
	OW_write_byte(SKIP_ROM);
	OW_write_byte(READ_SCRATCHPAD);

	uint8_t result[SCRATCHPAD_SIZE] = {0};
	for (int i = 0; i < SCRATCHPAD_SIZE; i++) {
		result[i] = OW_read_byte();
	}
	int16_t rawTemperature = ((int16_t) result[TEMP_MSB]) << 15; // Move the signed part to the MSB
	rawTemperature = rawTemperature | result[TEMP_LSB];

	// Get a higher resolution temperature as described in data sheet
	float temperature = 0.0;
	if (result[COUNT_PER_C] != 0) {
		temperature = (rawTemperature >> 1) - 0.25 + ((float) (result[COUNT_PER_C] - result[COUNT_REMAIN]) / result[COUNT_PER_C]);
	}
	return temperature;
}

void TS_print_temperature(float temperature, UART_HandleTypeDef* output) {
	if (temperature > MAX_TEMPERATURE || temperature < MIN_TEMPERATURE) {
		return;
	}
	// Format the temperature for UART output
	char outputString[16] = {"\0"};
	TS_get_formatted_temperature(outputString, temperature);
	UART_Send(output, outputString);
}

char* TS_get_formatted_temperature(char* buffer, float temperature) {
	strcat(buffer, "T: ");
	char valueBuffer[5] = {"\0"};
	itoa((uint16_t) temperature, valueBuffer, 10);
	strcat(buffer, valueBuffer);
	strcat(buffer, ".");
	memset(valueBuffer, 0, strlen(valueBuffer));
	itoa((uint16_t)(temperature * 100) % 100, valueBuffer, 10);
	strcat(buffer, valueBuffer);
	char degrees[] = {223, 'C', '\0'};
	strcat(buffer, degrees);
	if (!WD_within_range(&watchdogTemperature, (int8_t) temperature)) {
		strcat(buffer, "!");
	}
	return buffer;
}
