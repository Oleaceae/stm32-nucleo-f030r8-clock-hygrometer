#include "uart_send.h"

void UART_Send(UART_HandleTypeDef* huart, char* buffer) {
	HAL_UART_Transmit(huart, (uint8_t*) buffer, strlen((char*) buffer), HAL_MAX_DELAY);
}
