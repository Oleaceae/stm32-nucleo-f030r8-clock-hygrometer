#include "one_wire.h"

static void OW_write_lsb(uint8_t byte);
static uint8_t OW_read_bit();


bool OW_reset() {
	LL_GPIO_SetPinMode(TS_GPIO_Port, TS_Pin, LL_GPIO_MODE_OUTPUT);
	HAL_GPIO_WritePin(TS_GPIO_Port, TS_Pin, GPIO_PIN_RESET);
	delay_us(480);

	LL_GPIO_SetPinMode(TS_GPIO_Port, TS_Pin, LL_GPIO_MODE_INPUT);
	delay_us(100);
	uint8_t presence = HAL_GPIO_ReadPin(TS_GPIO_Port, TS_Pin);
	if (presence) {
		return false;
	}
	delay_us(380);
	return true;
}

void OW_write_byte(uint8_t byte) {
	for (uint8_t i = 0; i < 8; i++) {
		OW_write_lsb(byte);
		byte = byte >> 1;
	}
}

static void OW_write_lsb(uint8_t byte) {
	uint8_t lsb = byte & 0b1;

	LL_GPIO_SetPinMode(TS_GPIO_Port, TS_Pin, LL_GPIO_MODE_OUTPUT);
	HAL_GPIO_WritePin(TS_GPIO_Port, TS_Pin, GPIO_PIN_RESET);
	if (lsb) {
		delay_us(10);
		LL_GPIO_SetPinMode(TS_GPIO_Port, TS_Pin, LL_GPIO_MODE_INPUT);
		delay_us(50);
	}
	else {
		delay_us(60);
		LL_GPIO_SetPinMode(TS_GPIO_Port, TS_Pin, LL_GPIO_MODE_INPUT);
	}
	delay_us(1);
}

uint8_t OW_read_byte() {
	uint8_t byte = 0;
	for (uint8_t i = 0; i < 8; i++) {
		byte = byte >> 1;
		byte = byte | OW_read_bit() << 7;
	}
	return byte;
}

static uint8_t OW_read_bit() {
	LL_GPIO_SetPinMode(TS_GPIO_Port, TS_Pin, LL_GPIO_MODE_OUTPUT);
	HAL_GPIO_WritePin(TS_GPIO_Port, TS_Pin, GPIO_PIN_RESET);
	delay_us(1);
	LL_GPIO_SetPinMode(TS_GPIO_Port, TS_Pin, LL_GPIO_MODE_INPUT);
	delay_us(10);
	uint8_t state = HAL_GPIO_ReadPin(TS_GPIO_Port, TS_Pin);
	delay_us(50);
	return state;
}
