#ifndef MODULES_PIN_DEFINES_H_
#define MODULES_PIN_DEFINES_H_

#include "stm32f0xx_hal.h"

#define LCD_DB4_Pin GPIO_PIN_0
#define LCD_DB4_GPIO_Port GPIOC
#define LCD_DB5_Pin GPIO_PIN_1
#define LCD_DB5_GPIO_Port GPIOC
#define LCD_DB6_Pin GPIO_PIN_2
#define LCD_DB6_GPIO_Port GPIOC
#define LCD_DB7_Pin GPIO_PIN_3
#define LCD_DB7_GPIO_Port GPIOC
#define hygrometer_Pin GPIO_PIN_0
#define hygrometer_GPIO_Port GPIOA
#define USART_TX_Pin GPIO_PIN_2
#define USART_TX_GPIO_Port GPIOA
#define USART_RX_Pin GPIO_PIN_3
#define USART_RX_GPIO_Port GPIOA
#define TS_Pin GPIO_PIN_4
#define TS_GPIO_Port GPIOF
#define LD2_Pin GPIO_PIN_5
#define LD2_GPIO_Port GPIOA
#define KEYPAD_ROW_3_Pin GPIO_PIN_1
#define KEYPAD_ROW_3_GPIO_Port GPIOB
#define KEYPAD_ROW_3_EXTI_IRQn EXTI0_1_IRQn
#define KEYPAD_COL_3_Pin GPIO_PIN_11
#define KEYPAD_COL_3_GPIO_Port GPIOB
#define KEYPAD_COL_2_Pin GPIO_PIN_12
#define KEYPAD_COL_2_GPIO_Port GPIOB
#define KEYPAD_ROW_0_Pin GPIO_PIN_13
#define KEYPAD_ROW_0_GPIO_Port GPIOB
#define KEYPAD_ROW_0_EXTI_IRQn EXTI4_15_IRQn
#define KEYPAD_ROW_1_Pin GPIO_PIN_14
#define KEYPAD_ROW_1_GPIO_Port GPIOB
#define KEYPAD_ROW_1_EXTI_IRQn EXTI4_15_IRQn
#define KEYPAD_ROW_2_Pin GPIO_PIN_15
#define KEYPAD_ROW_2_GPIO_Port GPIOB
#define KEYPAD_ROW_2_EXTI_IRQn EXTI4_15_IRQn
#define KEYPAD_COL_1_Pin GPIO_PIN_11
#define KEYPAD_COL_1_GPIO_Port GPIOA
#define KEYPAD_COL_0_Pin GPIO_PIN_12
#define KEYPAD_COL_0_GPIO_Port GPIOA
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define LCD_E_Pin GPIO_PIN_6
#define LCD_E_GPIO_Port GPIOF
#define LCD_RS_Pin GPIO_PIN_7
#define LCD_RS_GPIO_Port GPIOF
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA

#endif
