#include "delay_us.h"

#include "stm32f0xx_hal.h"


extern TIM_HandleTypeDef htim6;

void delay_us(uint16_t microseconds) {
	__HAL_TIM_SET_COUNTER(&htim6, 0);
	while (__HAL_TIM_GET_COUNTER(&htim6) < microseconds) {
		// do nothing
	}

	/*
	 * Does the 1 mhz timer based microsecond delay work - Test with this. Breakpoint at stop and read times array's content.
	 * TODO debug code, only for presentation, remove
	uint16_t times[10] = {0};
	__HAL_TIM_SET_COUNTER(&htim6, 0);
	for (int i = 0; i < 10; i++) {
		times[i] = __HAL_TIM_GET_COUNTER(&htim6);
		HAL_Delay(i);
	}
	int stop = 0;
	*/
}
