#ifndef MODULES_HYGROMETER_H_
#define MODULES_HYGROMETER_H_

#include "stm32f0xx_hal.h"

// Output humidity as a formatted string on UART
// Output example: "(H: 45.3%)"
void HYG_print_humidity(uint16_t humidity, UART_HandleTypeDef* uart);

// Starts the ADC and reads its output
uint16_t HYG_get_humidity(ADC_HandleTypeDef* valueSource);

// Format humidity into a string of format "H: xxx.x%"
char* HYG_get_formatted_humidity(char* buffer, uint16_t value);

#endif
