#ifndef MODULES_WATCHDOG_H_
#define MODULES_WATCHDOG_H_

#include <stdbool.h>
#include <stdint.h>

// Only allow storing watchdog data in this page.
// Equal to 0x0800ffff - 0x400, which is 1 FLASH_PAGE_SIZE below the flash end address
#define WD_FLASH_PAGE_ADDRESS 0x800FC00
// Up until 4 bytes below the flash end address
#define WD_FLASH_MAXIMUM_ADDRESS 0x800FFFB

#define TEMPERATURE_WATCHDOG_ADDRESS WD_FLASH_PAGE_ADDRESS
#define HUMIDITY_WATCHDOG_ADDRESS (WD_FLASH_PAGE_ADDRESS + 0x4)

// 3 byte total, flash footprint will be 1 word (4 byte)
struct watchdog {
	bool active;
	int8_t min;
	int8_t max;
};

// Saving and reading a watchdog to and from FLASH memory
// Address must be a value between 0x0800FBFF and 0x0800FFFB - Within the final page in FLASH.
// Else no data is written or read
void WD_save_to_flash();
struct watchdog WD_load_from_flash(uint32_t address);

bool WD_within_range(struct watchdog* dog, int8_t value);
char* WD_get_formatted_values(char* buffer, struct watchdog* dog);
bool WD_compare(struct watchdog* dog, struct watchdog* other);
int8_t WD_clamp(int8_t value);

#endif
