#include "measuring_station.h"

#include <stdbool.h>

#include "hygrometer.h"
#include "temperature_sensor.h"
#include "lcd_16x2.h"
#include "one_wire.h"
#include "keypad.h"
#include "clock.h"
#include "watchdog.h"

extern ADC_HandleTypeDef hadc;
extern UART_HandleTypeDef huart2;
extern TIM_HandleTypeDef htim6;
extern TIM_HandleTypeDef htim14;
extern TIM_HandleTypeDef htim16;

extern struct watchdog watchdogTemperature;
extern struct watchdog watchdogHumidity;

enum Mode {
	TIME = 1,
	TIME_AND_TEMPERATURE = 2,
	TEMPERATURE_AND_HUMIDITY = 3,
	SWITCHING = 4,
	CONFIG = 5
};

enum ConfigMode {
	CONFIG_TIME = 1,
	CONFIG_ALARM_TEMPERATURE = 2,
	CONFIG_ALARM_HUMIDITY = 3
};

static void MS_init();
static void MS_display_welcome_message();
static void MS_display_temperature(uint8_t position);
static void MS_display_humidity(uint8_t position);
static void MS_display_time(uint8_t position);
static void MS_display_watchdog_state(uint8_t position, struct watchdog* dog);

// Display modes. Return value is the mode that's being switched into
static uint8_t MS_time();
static uint8_t MS_time_and_temperature();
static uint8_t MS_temperature_and_humidity();
static uint8_t MS_switching();

// Configuration menu functions
static uint8_t MS_config_menu();
static void MS_handle_time_config();
static void MS_handle_watchdog_config(struct watchdog* dog, uint32_t flashAddress);
static enum ConfigMode MS_switch_config_mode(int8_t step, enum ConfigMode currentMode);
static void MS_update_config_info_string(uint8_t position, enum ConfigMode mode);
static uint8_t MS_poll_keypad(uint16_t timeout);

// Time configuration functions
static ux_timestamp_s get_slot_time_value(uint8_t activeSlot);
static uint8_t handle_time_config_slot_change(uint8_t activeSlot, int8_t change);
static uint8_t time_slot_to_lcd_location(uint8_t activeSlot);

// Watchdog configuration functions
static void MS_handle_watchdog_value_change(int8_t change, uint8_t activeSlot, struct watchdog* dog);
static uint8_t handle_watchdog_config_slot_change(uint8_t activeSlot, int8_t change);
static uint8_t watchdog_slot_to_lcd_location(uint8_t activeSlot);

// If up/down are pressed, returns modeUp or modeDown.
// Return 0 when none of the required buttons were pressed
static uint8_t MS_keypad_poll_mode_change(uint8_t modeUp, uint8_t modeDown, uint16_t timeout);

#define UPDATE_INTERVAL_LONG_MS 10000	// Temperature & humidity only need to be measured every so often
#define UPDATE_INTERVAL_SHORT_MS 1000	// Clock requires an update every second
#define KEY_POLL_DELAY_MS 100			// Keypad is polled every x ms for a pressed key
#define NO_KEY_PRESSED 255				// Returned when polling for a key press times out

// Time configuration slots, format "24:00:00", each value relates to one part. Used for cursor jumping
#define SLOT_HOUR_10 0
#define SLOT_HOUR 1
#define SLOT_MINUTE_10 2
#define SLOT_MINUTE 3
#define SLOT_SECOND_10 4
#define SLOT_SECOND 5

// Watchdog configuration slots, format "OFF XX TO XX", each value relates to one part. Used for cursor jumping
#define SLOT_ACTIVE 0
#define SLOT_FROM_10 1
#define SLOT_FROM_1 2
#define SLOT_TO_10 3
#define SLOT_TO_1 4

static char temperaturePlaceholder[] = {'T', ':', ' ', 'X', '.', 'X', 223, 'C', '\0'};	// Extended ASCII character requires this type of initialization as they are platform dependent, 223 = °
static char humidityPlaceholder[] = "H: X.X%";
static enum CLK_Mode activeClockMode = CLK_MODE_FULL;


int MS_start() {
	MS_init();
	// MS_display_welcome_message();

	uint8_t mode = TIME;
	while (true) {
		switch (mode) {
		case TIME:
			mode = MS_time();
			break;
		case TIME_AND_TEMPERATURE:
			mode = MS_time_and_temperature();
			break;
		case TEMPERATURE_AND_HUMIDITY:
			mode = MS_temperature_and_humidity();
			break;
		case SWITCHING:
			mode = MS_switching();
			break;
		case CONFIG:
			mode = MS_config_menu();
			break;
		default:
			// Any mode returned 0 or a non existent mode? Load the default mode
			mode = MS_time();
			break;
		}

		/* UART debug prints
		HYG_print_humidity(HYG_get_humidity(&hadc), &huart2);
		UART_Send(&huart2, " - ");
		TS_print_temperature(TS_read_temperature(&huart2), &huart2);
		UART_Send(&huart2, " --- ");
		*/

		HAL_Delay(10);
	}

	return 0;
}

static void MS_init() {
	// Any peripheral initialization code
	HAL_TIM_Base_Start_IT(&htim6);
	HAL_TIM_Base_Start_IT(&htim14);
	HAL_TIM_Base_Start_IT(&htim16);

	watchdogTemperature = WD_load_from_flash(TEMPERATURE_WATCHDOG_ADDRESS);
	watchdogHumidity = WD_load_from_flash(HUMIDITY_WATCHDOG_ADDRESS);

	LCD_init();
}

static void MS_display_welcome_message() {
	// Fly in from the left, move around a bit, fly out to the right
	LCD_display_string_at("Welcome", 0x20);
	LCD_display_string_at("o . o", 0x61);

	for (int i = 0; i < 13; i++) {
		LCD_shift_right();
		HAL_Delay(200);
	}

	LCD_shift_left();
	HAL_Delay(500);
	LCD_shift_right();
	HAL_Delay(500);
	LCD_shift_right();
	HAL_Delay(500);
	LCD_shift_left();
	HAL_Delay(500);
	LCD_shift_left();
	HAL_Delay(500);

	for (int i = 0; i < 12; i++) {
		LCD_shift_right();
		HAL_Delay(200);
	}
	// Appear in the middle and blink a few times
	LCD_return_home();
	LCD_display_string_at("Welcome", 0x45);
	for (int i = 0; i < 5; i++) {
		LCD_display_string_at("o . o", 0x06);
		HAL_Delay(500);
		LCD_display_string_at("> . o", 0x06);
		HAL_Delay(300);
	}

	LCD_clear();
}

static uint8_t MS_time() {
	LCD_clear();

	while (true) {
		MS_display_time(LCD_FIRST_ROW_START_ADDR);

		uint8_t newMode = MS_keypad_poll_mode_change(SWITCHING, TIME_AND_TEMPERATURE, UPDATE_INTERVAL_SHORT_MS);
		if (newMode != NO_KEY_PRESSED) {
			return newMode;
		}
	}
	return 0;
}

static uint8_t MS_time_and_temperature() {
	LCD_clear();

	LCD_display_string_at(temperaturePlaceholder, LCD_SECOND_ROW_START_ADDR);
	// No need to update the temperature with the clock every second. Keep track of time passed so we only do it every so often. Update once immediately
	uint16_t timePassed = UPDATE_INTERVAL_LONG_MS;

	while (true) {
		MS_display_time(LCD_FIRST_ROW_START_ADDR);

		if (timePassed >= UPDATE_INTERVAL_LONG_MS) {
			MS_display_temperature(LCD_SECOND_ROW_START_ADDR);
			timePassed = 0;
		}

		uint8_t newMode = MS_keypad_poll_mode_change(TIME, TEMPERATURE_AND_HUMIDITY, UPDATE_INTERVAL_SHORT_MS);
		if (newMode != NO_KEY_PRESSED) {
			return newMode;
		}
		timePassed = timePassed + UPDATE_INTERVAL_SHORT_MS;
	}

	return 0;
}

static uint8_t MS_temperature_and_humidity() {
	LCD_clear();

	LCD_display_string(temperaturePlaceholder);
	LCD_display_string_at(humidityPlaceholder, LCD_SECOND_ROW_START_ADDR);
	LCD_return_home();


	while (true) {
		MS_display_temperature(LCD_FIRST_ROW_START_ADDR);
		MS_display_humidity(LCD_SECOND_ROW_START_ADDR);

		uint8_t newMode = MS_keypad_poll_mode_change(TIME_AND_TEMPERATURE, SWITCHING, UPDATE_INTERVAL_LONG_MS);
		if (newMode != NO_KEY_PRESSED) {
			return newMode;
		}
	}
	return 0;
}

static uint8_t MS_switching() {
	LCD_clear();

	bool displayTime = false;
	uint16_t currentUpdateInterval = UPDATE_INTERVAL_LONG_MS;
	uint16_t timePassed = 0;

	LCD_display_string(temperaturePlaceholder);
	LCD_display_string_at(humidityPlaceholder, LCD_SECOND_ROW_START_ADDR);
	LCD_return_home();

	while (true) {
		if (displayTime) {
			currentUpdateInterval = UPDATE_INTERVAL_SHORT_MS;

			MS_display_time(LCD_FIRST_ROW_START_ADDR);
			// Silently clear humidity from the second line. We don't want LCD_clear() as that displays a visibly clear screen for too long
			LCD_display_string_at("           ", LCD_SECOND_ROW_START_ADDR);
		}
		else {
			currentUpdateInterval = UPDATE_INTERVAL_LONG_MS;

			MS_display_temperature(LCD_FIRST_ROW_START_ADDR);
			MS_display_humidity(LCD_SECOND_ROW_START_ADDR);
		}

		uint8_t newMode = MS_keypad_poll_mode_change(TEMPERATURE_AND_HUMIDITY, TIME, currentUpdateInterval);
		if (newMode != NO_KEY_PRESSED) {
			return newMode;
		}
		timePassed = timePassed + currentUpdateInterval;
		// Make sure we only switch when the long interval passed. Ensures time is shown as long as temperature & humidity but updated every second
		if (timePassed >= UPDATE_INTERVAL_LONG_MS) {
			displayTime = !displayTime;
			if (displayTime) {
				// Clear any leftovers of the temperature string, else we may see "12:00:00C". Again, LCD_clear() would leave a visible time frame with nothing displayed.
				LCD_clear_range(LCD_FIRST_ROW_START_ADDR, 16);
			}
			timePassed = 0;
		}
	}

	return 0;
}

static uint8_t MS_keypad_poll_mode_change(uint8_t modeUp, uint8_t modeDown, uint16_t timeout) {
	uint8_t key = MS_poll_keypad(timeout);

	switch (key) {
	case KP_KEY_DOWN:
		return modeDown;
		break;
	case KP_KEY_UP:
		return modeUp;
		break;
	case KP_KEY_CONFIRM:
		return CONFIG;
		break;
	default:
		// Unused keys are discarded
		break;
	}
	return NO_KEY_PRESSED;
}

static void MS_display_time(uint8_t position) {
	static char buffer[16] = {'\0'};

	CLK_get_formatted_system_time(buffer, activeClockMode);

	LCD_display_string_at(buffer, position);
}

static void MS_display_temperature(uint8_t position) {
	static char buffer[12] = {'\0'};

	uint8_t tLength = strlen(buffer);
	memset(buffer, 0, strlen(buffer));

	TS_get_formatted_temperature(buffer, TS_read_temperature(&huart2));
	LCD_display_string_at(buffer, position);

	// If a string like "21.32°C" turns into "0.0°C", or we write over a humidity string, clear up enough possibly remaining characters by writing whitespace
	// Clearing the entire screen is slow and leads to a time where the screen is completely empty to the user. This way it happens nearly instantly
	uint8_t endOfStringAddress = position + strlen(buffer);
	if (strlen(buffer) < tLength) {
		LCD_clear_range(endOfStringAddress, endOfStringAddress + 3);
	}
}

static void MS_display_humidity(uint8_t position) {
	static char buffer[12] = {'\0'};

	uint8_t hLength = strlen(buffer);
	memset(buffer, 0, strlen(buffer));

	HYG_get_formatted_humidity(buffer, HYG_get_humidity(&hadc));
	LCD_display_string_at(buffer, position);

	uint8_t endOfStringAddress = position + strlen(buffer);
	if (strlen(buffer) < hLength) {
		LCD_clear_range(endOfStringAddress, endOfStringAddress + 3);
	}
}

uint8_t MS_config_menu() {

	enum ConfigMode mode = CONFIG_TIME;

	LCD_clear();
	LCD_show_cursor();

	const char info[] = "Config: ";
	uint8_t configStringOffset = strlen(info);

	LCD_display_string(info);
	MS_update_config_info_string(configStringOffset, mode);
	MS_display_time(LCD_SECOND_ROW_START_ADDR);
	LCD_cursor_jump(configStringOffset);

	while (true) {
		uint8_t key = MS_poll_keypad(UPDATE_INTERVAL_LONG_MS);

		if (key == NO_KEY_PRESSED) {
			continue;
		}

		switch (key) {
			case KP_KEY_CANCEL:
				LCD_hide_cursor();
				return 0;
				break;
			case KP_KEY_CONFIRM:
				switch (mode) {
					case CONFIG_TIME:
						MS_handle_time_config();
						break;
					case CONFIG_ALARM_TEMPERATURE:
						MS_handle_watchdog_config(&watchdogTemperature, TEMPERATURE_WATCHDOG_ADDRESS);
						break;
					case CONFIG_ALARM_HUMIDITY:
						MS_handle_watchdog_config(&watchdogHumidity, HUMIDITY_WATCHDOG_ADDRESS);
						break;
				}
				break;
			case KP_KEY_LEFT:
				mode = MS_switch_config_mode(-1, mode);
				MS_update_config_info_string(configStringOffset, mode);
				break;
			case KP_KEY_RIGHT:
				mode = MS_switch_config_mode(1, mode);
				MS_update_config_info_string(configStringOffset, mode);
				break;
			case KP_KEY_UP:
				if (mode == CONFIG_TIME) {
					activeClockMode = activeClockMode == CLK_MODE_FULL ? CLK_MODE_AM_PM : CLK_MODE_FULL;
					MS_update_config_info_string(configStringOffset, mode);
				}
				break;
			case KP_KEY_DOWN:
				if (mode == CONFIG_TIME) {
					activeClockMode = activeClockMode == CLK_MODE_FULL ? CLK_MODE_AM_PM : CLK_MODE_FULL;
					MS_update_config_info_string(configStringOffset, mode);
				}
				break;
			default:
				break;
		}
		LCD_cursor_jump(configStringOffset);	// Return the cursor when a key was pressed and the user cancelled or confirmed their choices
	}

	return 0;
}

void MS_handle_time_config() {
	ux_timestamp_s cache = CLK_get();
	char buffer[16] = {'\0'};

	CLK_get_formatted_time(buffer, cache, activeClockMode);
	LCD_display_string_at(buffer, LCD_SECOND_ROW_START_ADDR);
	LCD_cursor_jump(LCD_SECOND_ROW_START_ADDR);

	uint8_t activeSlot = 0;

	while (true) {
		uint8_t key = MS_poll_keypad(UPDATE_INTERVAL_LONG_MS);

		if (key == NO_KEY_PRESSED) {
			continue;
		}

		switch (key) {
			case KP_KEY_CANCEL:
				MS_display_time(LCD_SECOND_ROW_START_ADDR);
				return;
			case KP_KEY_CONFIRM:
				CLK_set(cache);
				return;
			case KP_KEY_UP:
				cache = cache + get_slot_time_value(activeSlot);
				break;
			case KP_KEY_DOWN:
				cache = cache - get_slot_time_value(activeSlot);
				break;
			case KP_KEY_LEFT:
				activeSlot = handle_time_config_slot_change(activeSlot, -1);
				break;
			case KP_KEY_RIGHT:
				activeSlot = handle_time_config_slot_change(activeSlot, 1);
				break;
			default:
				break;
		}
		CLK_get_formatted_time(buffer, cache, activeClockMode);
		LCD_display_string_at(buffer, LCD_SECOND_ROW_START_ADDR);
		LCD_cursor_jump(time_slot_to_lcd_location(activeSlot));
	}
}

ux_timestamp_s get_slot_time_value(uint8_t activeSlot) {
	switch (activeSlot) {
	case SLOT_HOUR_10:
		return 36000;
	case SLOT_HOUR:
		return 3600;
	case SLOT_MINUTE_10:
		return 600;
	case SLOT_MINUTE:
		return 60;
	case SLOT_SECOND_10:
		return 10;
	case SLOT_SECOND:
		return 1;
	default:
		return 0;
	}
}

uint8_t handle_time_config_slot_change(uint8_t activeSlot, int8_t change) {
	if (change < -1 || change > 1) {
		return activeSlot;
	}

	if (activeSlot == SLOT_HOUR_10 && change == -1) {
		return SLOT_SECOND;
	}
	if (activeSlot == SLOT_SECOND && change == 1) {
		return SLOT_HOUR_10;
	}

	uint8_t newSlot = activeSlot + change;
	return newSlot;
}

uint8_t time_slot_to_lcd_location(uint8_t activeSlot) {
	switch (activeSlot) {
		case SLOT_HOUR_10:
			return LCD_SECOND_ROW_START_ADDR;
		case SLOT_HOUR:
			return LCD_SECOND_ROW_START_ADDR + 1;
		case SLOT_MINUTE_10:
			return LCD_SECOND_ROW_START_ADDR + 3;
		case SLOT_MINUTE:
			return LCD_SECOND_ROW_START_ADDR + 4;
		case SLOT_SECOND_10:
			return LCD_SECOND_ROW_START_ADDR + 6;
		case SLOT_SECOND:
			return LCD_SECOND_ROW_START_ADDR + 7;
		default:
			return LCD_SECOND_ROW_START_ADDR;
	}
}

void MS_handle_watchdog_config(struct watchdog* dog, uint32_t flashAddress) {
	LCD_cursor_jump(LCD_SECOND_ROW_START_ADDR);
	struct watchdog cache = *dog;

	char buffer[17] = {'\0'};
	uint8_t activeSlot = 0;

	while (true) {
		uint8_t key = MS_poll_keypad(UPDATE_INTERVAL_LONG_MS);

		if (key == NO_KEY_PRESSED) {
			continue;
		}

		switch (key) {
			case KP_KEY_CANCEL:
				MS_display_watchdog_state(LCD_SECOND_ROW_START_ADDR, dog);
				return;
			case KP_KEY_CONFIRM:
				if (!WD_compare(&cache, dog)) {
					*dog = cache;
					WD_save_to_flash();
				}
				return;
			case KP_KEY_UP:
				MS_handle_watchdog_value_change(1, activeSlot, &cache);
				break;
			case KP_KEY_DOWN:
				MS_handle_watchdog_value_change(-1, activeSlot, &cache);
				break;
			case KP_KEY_LEFT:
				activeSlot = handle_watchdog_config_slot_change(activeSlot, -1);
				break;
			case KP_KEY_RIGHT:
				activeSlot = handle_watchdog_config_slot_change(activeSlot, 1);
				break;
			default:
				break;
		}
		WD_get_formatted_values(buffer, &cache);
		LCD_display_string_at(buffer, LCD_SECOND_ROW_START_ADDR);
		LCD_cursor_jump(watchdog_slot_to_lcd_location(activeSlot));
	}
}

void MS_handle_watchdog_value_change(int8_t change, uint8_t activeSlot, struct watchdog* dog) {
	switch (activeSlot) {
		case SLOT_ACTIVE:
			dog->active = !dog->active;
			break;
		case SLOT_FROM_10:
			dog->min = WD_clamp(dog->min + (change == 1 ? 10 : -10));
			break;
		case SLOT_FROM_1:
			dog->min = WD_clamp(dog->min + (change == 1 ? 1 : -1));
			break;
		case SLOT_TO_10:
			dog->max = WD_clamp(dog->max + (change == 1 ? 10 : -10));
			break;
		case SLOT_TO_1:
			dog->max = WD_clamp(dog->max + (change == 1 ? 1 : -1));
			break;
	}
}

uint8_t handle_watchdog_config_slot_change(uint8_t activeSlot, int8_t change) {
	if (change < -1 || change > 1) {
		return activeSlot;
	}

	if (activeSlot == SLOT_ACTIVE && change == -1) {
		return SLOT_TO_1;
	}
	if (activeSlot == SLOT_TO_1 && change == 1) {
		return SLOT_ACTIVE;
	}

	uint8_t newSlot = activeSlot + change;
	return newSlot;
}

uint8_t watchdog_slot_to_lcd_location(uint8_t activeSlot) {
	switch (activeSlot) {
		case SLOT_ACTIVE:
			return LCD_SECOND_ROW_START_ADDR;
		case SLOT_FROM_10:
			return LCD_SECOND_ROW_START_ADDR + 4;
		case SLOT_FROM_1:
			return LCD_SECOND_ROW_START_ADDR + 5;
		case SLOT_TO_10:
			return LCD_SECOND_ROW_START_ADDR + 10;
		case SLOT_TO_1:
			return LCD_SECOND_ROW_START_ADDR + 11;
		default:
			return LCD_SECOND_ROW_START_ADDR;
	}
}

static enum ConfigMode MS_switch_config_mode(int8_t step, enum ConfigMode currentMode) {
	if (step < -1 || step > 1) {
		return currentMode;
	}

	// Handle mode under- and overflow
	if (step == -1 && currentMode == CONFIG_TIME) {
		return CONFIG_ALARM_HUMIDITY;
	}
	else if (step == 1 && currentMode == CONFIG_ALARM_HUMIDITY) {
		return CONFIG_TIME;
	}
	else {
		return (currentMode + step);
	}
}

static void MS_update_config_info_string(uint8_t position, enum ConfigMode mode) {
	LCD_clear_range(position, position + 10);
	switch(mode) {
		case CONFIG_TIME:
			char timeMode[12] = "Time";
			if (activeClockMode == CLK_MODE_FULL) {
				strcat(timeMode, " 24h");
			}
			else {
				strcat(timeMode, " 12h");
			}

			LCD_display_string_at(timeMode, position);
			LCD_clear_range(LCD_SECOND_ROW_START_ADDR, LCD_SECOND_ROW_START_ADDR + 15);
			MS_display_time(LCD_SECOND_ROW_START_ADDR);
			break;
		case CONFIG_ALARM_TEMPERATURE:
			char alarmC[] = {'A', 'l', 'a', 'r', 'm', ' ', 223, 'C', '\0'};
			LCD_display_string_at(alarmC, position);
			LCD_clear_range(LCD_SECOND_ROW_START_ADDR, LCD_SECOND_ROW_START_ADDR + 15);
			MS_display_watchdog_state(LCD_SECOND_ROW_START_ADDR, &watchdogTemperature);
			break;
		case CONFIG_ALARM_HUMIDITY:
			LCD_display_string_at("Alarm H%", position);
			LCD_clear_range(LCD_SECOND_ROW_START_ADDR, LCD_SECOND_ROW_START_ADDR + 15);
			MS_display_watchdog_state(LCD_SECOND_ROW_START_ADDR, &watchdogHumidity);
			break;
	}
}

void MS_display_watchdog_state(uint8_t position, struct watchdog* dog) {
	char buffer[20] = {'\0'};
	WD_get_formatted_values(buffer, dog);
	LCD_display_string_at(buffer, position);
}

uint8_t MS_poll_keypad(uint16_t timeout) {
	uint16_t timeWaited = 0;
	while (timeWaited < timeout) {
		if (KP_poll()) {
			return KP_consume_key();
		}
		else {
			timeWaited = timeWaited + KEY_POLL_DELAY_MS;
			HAL_Delay(KEY_POLL_DELAY_MS);
		}
	}
	return NO_KEY_PRESSED;
}
