#ifndef MODULES_KEYPAD_H_
#define MODULES_KEYPAD_H_

#include <stdbool.h>
#include <stdint.h>

#define KP_KEY_CONFIRM 0
#define KP_KEY_CANCEL 4
#define KP_KEY_UP 2
#define KP_KEY_LEFT 5
#define KP_KEY_RIGHT 7
#define KP_KEY_DOWN 10

bool KP_poll();
uint8_t KP_consume_key();
void KP_debounce();

// Handle detected keypress
void KP_handle_keypress(uint16_t GPIO_Pin);

// Turning keypad columns on/off
void KP_multiplexing_switch();

#endif
