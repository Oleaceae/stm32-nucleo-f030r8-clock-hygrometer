#include "hygrometer.h"

#include <stdlib.h>
#include <string.h>

#include "uart_send.h"
#include "watchdog.h"

#define HYGROMETER_MAX_VALUE 4095 // Potentiometer has a value range of 12 bit

struct watchdog watchdogHumidity = {0, 0, 0};

uint16_t HYG_get_humidity(ADC_HandleTypeDef* valueSource) {
	HAL_ADC_Start(valueSource);
	HAL_ADC_PollForConversion(valueSource, 1);
	return HAL_ADC_GetValue(valueSource);
}

void HYG_print_humidity(uint16_t humidity, UART_HandleTypeDef* output) {
	if (humidity > HYGROMETER_MAX_VALUE) {
		return;
	}
	char humidityString[16] = {"\0"};
	HYG_get_formatted_humidity(humidityString, humidity);
	UART_Send(output, humidityString);
}

char* HYG_get_formatted_humidity(char* buffer, uint16_t value) {
	if (value > HYGROMETER_MAX_VALUE) {
		return buffer;
	}
	strcat(buffer, "H: ");
	float ratio = ((float) value / HYGROMETER_MAX_VALUE);
	char valueBuffer[4];
	itoa((uint16_t) (ratio * 100), valueBuffer, 10);
	strcat(buffer, valueBuffer);
	strcat(buffer, ".");
	memset(valueBuffer, 0, strlen(valueBuffer));
	itoa((uint16_t) (ratio * 1000) % 10, valueBuffer, 10);
	strcat(buffer, valueBuffer);
	strcat(buffer, "%");
	if (!WD_within_range(&watchdogHumidity, (int8_t) (ratio * 100))) {
		strcat(buffer, "!");
	}
	return buffer;
}
