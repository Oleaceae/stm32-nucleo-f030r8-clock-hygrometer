#include "lcd_16x2.h"

#define DEFAULT_WAIT_TIME_US 50
#define DEFAULT_WAIT_TIME_MS_SHORT 7
#define DEFAULT_WAIT_TIME_MS_LONG 400
#define WAIT_TIME_POWER_UP 2500


// Setting the display mode
static void LCD_set_8_bit();
static void LCD_set_4_bit();

// Sets the scrolling mode
static void LCD_config_entry_mode();
// Turns on both rows with a font size of 5*8
static void LCD_init_function();

// Send data depending on 4-/8-bit mode
static void LCD_send_4_bit_mode(uint8_t data);
static void LCD_send_8_bit_mode(uint8_t data);

static void LCD_send_nibble(uint8_t data);

void LCD_init() {
	HAL_GPIO_WritePin(LCD_E_GPIO_Port, LCD_E_Pin, GPIO_PIN_RESET);
	HAL_Delay(WAIT_TIME_POWER_UP);
	LCD_set_8_bit();
	LCD_set_8_bit();
	LCD_set_4_bit();
	LCD_init_function();
	LCD_off();
	LCD_clear();
	LCD_config_entry_mode();
	LCD_on();
	LCD_return_home();
}

void LCD_on() {
	HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, GPIO_PIN_RESET);
	LCD_send_4_bit_mode(0b00001100);
	delay_us(DEFAULT_WAIT_TIME_US);
}

void LCD_hide_cursor() {
	HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, GPIO_PIN_RESET);
	LCD_send_4_bit_mode(0b00001100);
	delay_us(DEFAULT_WAIT_TIME_US);
}

void LCD_show_cursor() {
	HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, GPIO_PIN_RESET);
	LCD_send_4_bit_mode(0b00001111);
	delay_us(DEFAULT_WAIT_TIME_US);
}

void LCD_off() {
	HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, GPIO_PIN_RESET);
	LCD_send_4_bit_mode(0b00001000);
	delay_us(DEFAULT_WAIT_TIME_US);
}

void LCD_clear() {
	HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, GPIO_PIN_RESET);
	LCD_send_4_bit_mode(0b00000001);
	HAL_Delay(DEFAULT_WAIT_TIME_MS_LONG);
}

void LCD_clear_range(const uint8_t start, const uint8_t end) {
	char str[end - start];
	memset(str, ' ', sizeof(str));
	LCD_display_string_at(str, start);
}

void LCD_display_string(const char* string) {
	HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, GPIO_PIN_SET);
	delay_us(DEFAULT_WAIT_TIME_US);
	for (int i = 0; i < strlen(string); i++) {
		LCD_send_4_bit_mode(string[i]);
	}
	HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, GPIO_PIN_RESET);
	HAL_Delay(DEFAULT_WAIT_TIME_MS_SHORT);
}

void LCD_cursor_jump(const uint8_t location) {
	HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, GPIO_PIN_RESET);
	LCD_send_4_bit_mode(0b10000000 | location);
	delay_us(DEFAULT_WAIT_TIME_US);
}

void LCD_display_string_at(const char* string, const uint8_t start) {
	LCD_cursor_jump(start);
	LCD_display_string(string);
}

void LCD_display_char(const char character) {
	const char str[2] = {character, '\0'};
	LCD_display_string(str);
}

void LCD_display_char_at(const char character, const uint8_t position) {
	LCD_cursor_jump(position);
	LCD_display_char(character);
}

void LCD_cursor_to_second_row() {
	HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, GPIO_PIN_RESET);
	LCD_send_4_bit_mode(0b11000000);
	delay_us(DEFAULT_WAIT_TIME_US);
}

void LCD_shift_cursor_right() {
	HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, GPIO_PIN_RESET);
	LCD_send_4_bit_mode(0b00010100);
	HAL_Delay(1000);
	delay_us(DEFAULT_WAIT_TIME_US);
}

void LCD_shift_cursor_left() {
	HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, GPIO_PIN_RESET);
	LCD_send_4_bit_mode(0b00010000);
	delay_us(DEFAULT_WAIT_TIME_US);
}

void LCD_shift_left() {
	HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, GPIO_PIN_RESET);
	LCD_send_4_bit_mode(0b00011000);
	delay_us(DEFAULT_WAIT_TIME_US);
}

void LCD_shift_right() {
	HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, GPIO_PIN_RESET);
	LCD_send_4_bit_mode(0b00011100);
	delay_us(DEFAULT_WAIT_TIME_US);
}

void LCD_return_home() {
	HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, GPIO_PIN_RESET);
	LCD_send_4_bit_mode(0b00000010);
	HAL_Delay(DEFAULT_WAIT_TIME_MS_SHORT);
}

static void LCD_set_8_bit() {
	HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, GPIO_PIN_RESET);
	LCD_send_8_bit_mode(0b00110000);
	delay_us(DEFAULT_WAIT_TIME_US);
}

static void LCD_set_4_bit() {
	HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, GPIO_PIN_RESET);
	LCD_send_8_bit_mode(0b00100000);
	delay_us(DEFAULT_WAIT_TIME_US);
}

static void LCD_config_entry_mode() {
	HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, GPIO_PIN_RESET);
	LCD_send_4_bit_mode(0b00000110);
	delay_us(DEFAULT_WAIT_TIME_US);
}

static void LCD_init_function() {
	HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, GPIO_PIN_RESET);
	LCD_send_4_bit_mode(0b00101000);
	delay_us(DEFAULT_WAIT_TIME_US);
}

static void LCD_send_4_bit_mode(const uint8_t data) {
	const uint8_t low = data & 0x0F;
	const uint8_t high = data  & 0xF0;

	LCD_send_nibble(high >> 4);
	LCD_send_nibble(low);
}

// Does not use the lower 4 bit - we are not connected to DB0 through 3.
// This is only used for initialization where the lower 4 bit are unused when switching modes anyway.
static void LCD_send_8_bit_mode(const uint8_t data) {
	const uint8_t high = data & 0xF0;
	LCD_send_nibble(high >> 4);
}

static void LCD_send_nibble(const uint8_t data) {
	HAL_GPIO_WritePin(LCD_E_GPIO_Port, LCD_E_Pin, GPIO_PIN_RESET);

	HAL_GPIO_WritePin(LCD_DB7_GPIO_Port, LCD_DB7_Pin, data & 8);
	HAL_GPIO_WritePin(LCD_DB6_GPIO_Port, LCD_DB6_Pin, data & 4);
	HAL_GPIO_WritePin(LCD_DB5_GPIO_Port, LCD_DB5_Pin, data & 2);
	HAL_GPIO_WritePin(LCD_DB4_GPIO_Port, LCD_DB4_Pin, data & 1);

	delay_us(DEFAULT_WAIT_TIME_US);

	HAL_GPIO_WritePin(LCD_E_GPIO_Port, LCD_E_Pin, GPIO_PIN_SET);

	delay_us(DEFAULT_WAIT_TIME_US);
	HAL_GPIO_WritePin(LCD_E_GPIO_Port, LCD_E_Pin, GPIO_PIN_RESET);
	delay_us(DEFAULT_WAIT_TIME_US);
}
