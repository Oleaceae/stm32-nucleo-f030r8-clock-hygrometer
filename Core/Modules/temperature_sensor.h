#ifndef MODULES_TEMPERATURE_SENSOR_H_
#define MODULES_TEMPERATURE_SENSOR_H_

#include <stdio.h>

#include "one_wire.h"
#include "uart_send.h"
#include "pin_defines.h"


float TS_read_temperature(UART_HandleTypeDef* debugOutput);
void TS_print_temperature(float temperature, UART_HandleTypeDef* output);
char* TS_get_formatted_temperature(char* buffer, float temperature);

#endif
