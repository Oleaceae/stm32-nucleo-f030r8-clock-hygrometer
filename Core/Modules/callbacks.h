#ifndef MODULES_CALLBACKS_H_
#define MODULES_CALLBACKS_H_

#include "stm32f0xx_hal.h"

// Timer IRQ handler
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim);
// GPIO external interrupt handler
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin);

#endif
