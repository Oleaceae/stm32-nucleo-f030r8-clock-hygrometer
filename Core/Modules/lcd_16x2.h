#ifndef MODULES_LCD_16X2_H_
#define MODULES_LCD_16X2_H_

#include <string.h>

#include "delay_us.h"
#include "pin_defines.h"

#define LCD_FIRST_ROW_START_ADDR 0x0
#define LCD_SECOND_ROW_START_ADDR 0x40

void LCD_init();
void LCD_on();
void LCD_off();
void LCD_clear();
void LCD_clear_range(const uint8_t start, const uint8_t end);
void LCD_display_string(const char* string);
void LCD_display_string_at(const char* string, const uint8_t start);
void LCD_display_char(const char character);
void LCD_display_char_at(const char character, const uint8_t position);
void LCD_cursor_jump(const uint8_t location);
void LCD_shift_cursor_right();
void LCD_shift_cursor_left();
void LCD_hide_cursor();
void LCD_show_cursor();
void LCD_shift_left();
void LCD_shift_right();
void LCD_return_home();
void LCD_cursor_to_second_row();


#endif
