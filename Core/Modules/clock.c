#include "clock.h"

#include <string.h>
#include <stdlib.h>

static ux_timestamp_s time = {0};
static char delimiter[] = ":";
static char am[] = " AM";
static char pm[] = " PM";
static char pad[] = "0";

static char* format_time_full(char* buffer, ux_timestamp_s timestamp);
static char* format_time_am_pm(char* buffer, ux_timestamp_s timestamp);
static char* format_time(char* buffer, ux_timestamp_s timestamp, uint32_t secondsInDay);

#define SECONDS_IN_DAY 86400
#define SECONDS_IN_DAY_HALF 43200
#define SECONDS_IN_HOUR 3600
#define SECONDS_IN_MINUTE 60


ux_timestamp_s CLK_get() {
	return time;
}

void CLK_set(ux_timestamp_s newTime) {
	time = newTime;
}

void CLK_tick() {
	time = time + 1;
}

char* CLK_get_formatted_system_time(char* buffer, enum CLK_Mode mode) {
	return CLK_get_formatted_time(buffer, time, mode);
}

char* CLK_get_formatted_time(char* buffer, ux_timestamp_s timestamp, enum CLK_Mode mode) {
	switch (mode) {
	case CLK_MODE_FULL:
		format_time_full(buffer, timestamp);
		break;
	case CLK_MODE_AM_PM:
		format_time_am_pm(buffer, timestamp);
		break;
	case CLK_MODE_RAW:
		itoa(time, buffer, 10);
		break;
	}
	return buffer;
}

static char* format_time_full(char* buffer, ux_timestamp_s timestamp) {
	format_time(buffer, timestamp, SECONDS_IN_DAY);

	return buffer;
}

static char* format_time_am_pm(char* buffer, ux_timestamp_s timestamp) {
	format_time(buffer, timestamp, SECONDS_IN_DAY_HALF);
	if (((timestamp % SECONDS_IN_DAY) / SECONDS_IN_HOUR) >= 12) {
		strcat(buffer, pm);
	}
	else {
		strcat(buffer, am);
	}

	return buffer;
}

static char* format_time(char* buffer, ux_timestamp_s timestamp, uint32_t dayMode) {
	memset(buffer, 0, strlen(buffer));
	ux_timestamp_s timeToday = timestamp % dayMode;
	uint32_t hours = timeToday / SECONDS_IN_HOUR;
	uint32_t minutes = (timeToday % SECONDS_IN_HOUR) / SECONDS_IN_MINUTE;
	uint32_t seconds = (timeToday % SECONDS_IN_HOUR) % SECONDS_IN_MINUTE;
	if (hours < 10) {
		strcat(buffer, pad);
	}
	itoa(hours, buffer + strlen(buffer), 10);
	strcat(buffer, delimiter);
	if (minutes < 10) {
		strcat(buffer, pad);
	}
	itoa(minutes, buffer + strlen(buffer), 10);
	strcat(buffer, delimiter);
	if (seconds < 10) {
		strcat(buffer, pad);
	}
	itoa(seconds, buffer + strlen(buffer), 10);

	return buffer;
}
