#ifndef MODULES_UART_SEND_H_
#define MODULES_UART_SEND_H_

#include <string.h>

#include "stm32f0xx_hal.h"


void UART_Send(UART_HandleTypeDef* huart, char* buffer);

#endif
